#!/usr/bin/perl

use strict; 
use warnings;

### Change THIS!
my $required_endpoints = 5;



my $max_counter = 4;
my $counter = 0;
my $number = 0;

print "trbreset_loop.pl!\n";

#js while (($number != 65) || ($counter > $max_counter)) {
while (($number != $required_endpoints) && ($counter < $max_counter)) {
    my $c; my $res;

    $counter++;
    $c= "trbcmd reset";
    $res = qx($c);

    #print "reset: [$res]\n";

    $c = "trbcmd i 0xffff 2>&1";
    $res = qx($c);

    #print "i: [$res]\n";

    if ($res =~ /Timeout/) {
      print "Timeout communicating with the TRB3!\n";
      system("/usr/bin/killall trbnetd");
      system("/usr/bin/killall -KILL trbnetd");
      system("/usr/bin/killall trbcmd");
      system("/usr/bin/killall -KILL trbcmd");
      last;
    }

    $c = "trbcmd i 0xffff | wc -l";
    $res = qx($c),
    print "- number of trb endpoints in the system: $res";
    ($number) = $res =~ /(\d+)/;
    print "number of enpoints is not equal to the required enpoints $required_endpoints, so try next reset!\n" if ($number !=$required_endpoints);
}

print "trbreset_loop.pl: finished!\n";
exit 0;

