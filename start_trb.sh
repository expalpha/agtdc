#!/bin/sh
source ~/.bashrc

echo Starting TRB3 TDC setup
killall trbreset_loop.pl
killall trbcmd
killall trbnetd
sleep 2
killall -KILL trbcmd
killall -KILL trbnetd
sleep 2
echo "CD"
cd /zssd/home1/agtdc/trbsoft/daqtools/users/triumf_trb171
echo $DAQ_TOOLS_PATH
pwd
source startup.sh
echo "FINSH STARTUP"
trbcmd i 0xffff
trbcmd r 0xc001 0xd300

#end

